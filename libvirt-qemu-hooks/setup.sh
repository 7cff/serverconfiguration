#!/bin/bash

# One liner for getting the directory the script resides in.
SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"

ln -s ${SCRIPT_DIR}/qemu.sh /etc/libvirt/hooks/qemu
ln -s ${SCRIPT_DIR}/add-port.sh /usr/local/bin/add-port
