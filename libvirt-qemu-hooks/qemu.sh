#!/bin/bash
GUEST_ADDR=192.168.1.2
GUEST_NAME=node1
VIR_IFACE=virbr1
PORT_CONFIG=${PORT_CONFIG:-/etc/vm_ports.conf}

if [ "${1}" = "${GUEST_NAME}" ]; then
	echo "`date`" hook/qemu "${1}" "${2}" >>/root/hook.log
	if [ "${2}" = "stopped" ] || [ "${2}" = "reconnect" ]; then
		while IFS='' read -r mappings || [[ -n "$mappings" ]]; do
			echo "kvm-Ho." >> /root/hook.log
			IFS=" " read -r -a ports <<< "$mappings"
			/usr/sbin/iptables -D FORWARD -o ${VIR_IFACE} -d ${GUEST_ADDR} -j ACCEPT
			/usr/sbin/iptables -t nat -D PREROUTING -p ${ports[0]} --dport ${ports[1]} -j DNAT --to ${GUEST_ADDR}:${ports[2]}
		done < "$PORT_CONFIG"
	fi
	if [ "${2}" = "start" ] || [ "${2}" = "reconnect" ]; then
		while IFS='' read -r mappings || [[ -n "$mappings" ]]; do
			echo "kvm-Hey." >> /root/hook.log
			IFS=" " read -r -a ports <<< "$mappings"
			/usr/sbin/iptables -I FORWARD -o ${VIR_IFACE} -d ${GUEST_ADDR} -j ACCEPT
			/usr/sbin/iptables -t nat -I PREROUTING -p ${ports[0]} --dport ${ports[1]} -j DNAT --to ${GUEST_ADDR}:${ports[2]}
		done < "$PORT_CONFIG"
	fi
fi

if [ "${1}" = "debug" ]; then
	echo "Running in debug mode. Reading mappings."
	echo "Guest has address ${GUEST_ADDR} and name ${GUEST_NAME} on interface ${VIR_IFACE}."
	echo "Port config located at ${PORT_CONFIG}."
	while IFS='' read -r mappings || [[ -n "$mappings" ]]; do
		IFS=" " read -r -a ports <<< "$mappings"
		echo "${ports[0]} ${ports[1]} on host is mapped to ${ports[2]} on guest"
	done < "$PORT_CONFIG"
fi
